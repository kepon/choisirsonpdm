const userSettings = {
  "form_default": {
    "level":     1,
    "temp_base_years_archive":     20,
    "ubat_global":                 0.3,
    "venti_global":                0.14,
    "g":                           0.3,
    "livingvolume_auto" :           false,
    "livingvolume":                 92,
    "livingspace":                  40,
    "livingheight":               2.3,
    "wastagesurface":           200,
    "temp_indor":                 18, 
    "temp_base_auto":             true,
    "transparent":                  true,
    "nav-tab-record":             "nav-cartenf-tab",
  },
//  "includeJavascript" : {
//    "piwik2" : "include/piwik2.js",
//    "log-table" : "https://omailgw.retzo.net/js/log-table.js",
//  },
}
