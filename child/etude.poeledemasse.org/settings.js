const userSettings = {
  "form_default": {
    "level":                    3,
    "wastagesurface":           200,
    "temp_indor":               19, 
    "temp_base_auto":           true,
    "nav-tab-record":           "nav-cartenf-tab",
    "temp_ground":                 11,
    "temp_base_years_archive":     20,
    "dju_years_archive":           20,
  },
}
